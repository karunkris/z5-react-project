import { BrowserRouter } from "react-router-dom";

import { AuthProvider } from "./other/AuthContext";
import Routing from "./other/Routing";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <AuthProvider>
          <Routing />
        </AuthProvider>
      </BrowserRouter>
    </div>
  );
};

export default App;
