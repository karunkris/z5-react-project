import { doc, updateDoc } from "firebase/firestore";
import EditCard from "../components/EditCard";
import { db } from "../other/firebase";
import { useAuth } from "../other/AuthContext";
import { useNavigate } from "react-router-dom";

const EditPage = () => {
  const { activeUser } = useAuth();

  let navigate = useNavigate();

  const updateUserProfile = (updatedData) => {
    const docRef = doc(db, "employees", activeUser.uid);
    updateDoc(docRef, updatedData).then(() => {
      navigate("/home");
    });
  };
  return (
    <div className="mainBackground">
      <EditCard onUpdate={updateUserProfile} />
    </div>
  );
};

export default EditPage;
