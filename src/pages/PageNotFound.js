const PageNotFound = () => {
  return <h2 className="pageNotFound">This Page Doesn't Exist</h2>;
};
export default PageNotFound;
