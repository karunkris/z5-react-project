import LoginCard from "../components/LoginCard";
import { useAuth } from "../other/AuthContext";
import { useState } from "react/cjs/react.development";
import { useNavigate } from "react-router-dom";
import Mounts from "../other/Mounts";

const LoginPage = () => {
  const [loading, setLoading] = useState(false);

  const { loginUser } = useAuth();

  const mounted = Mounts();

  let navigate = useNavigate();

  const userLogin = async (email, password) => {
    setLoading(true);

    try {
      await loginUser(email, password);
      navigate("/home");
    } catch (error) {
      alert(error.message);
    }
    mounted.current && setLoading(false);
  };

  return (
    <div className="mainBackground">
      <LoginCard onLogin={userLogin} loading={loading} />
    </div>
  );
};

export default LoginPage;
