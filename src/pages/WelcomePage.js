import { Link } from "react-router-dom";

const linkStyle = {
  textDecoration: "none",
  color: "inherit",
};

const WelcomePage = () => {
  return (
    <div className="mainBackground">
      <div className="welcomeContainer">
        <h2 className="welcomeText">Employee Directory</h2>
        <button className="getStarted">
          <Link to={"/login"} style={linkStyle}>
            Get Started
          </Link>
        </button>
        <p className="welcomeSubtext">
          <Link to={"/signup"} style={linkStyle}>
            Signup if you don't have an account
          </Link>
        </p>
      </div>
    </div>
  );
};

export default WelcomePage;
