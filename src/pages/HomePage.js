import { useEffect, useState } from "react";
import EmployeeDetails from "../components/EmployeeDetails";
import HomeNavbar from "../components/HomeNavbar";
import { db } from "../other/firebase";
import { collection, getDocs } from "firebase/firestore";

const HomePage = () => {
  const [users, setUsers] = useState([]);
  const userCollection = collection(db, "employees");

  useEffect(() => {
    const getUsers = async () => {
      const userData = await getDocs(userCollection);
      setUsers(userData.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };

    getUsers();
  }, []);

  return (
    <>
      <HomeNavbar />
      <h2 className="homeText">Employee Directory</h2>
      <div className="d-flex gap-4 mt-4 flex-wrap justify-content-center">
        {users.map((user) => {
          return (
            <div key={user.id}>
              <EmployeeDetails
                name={user.name}
                skills={user.skills}
                age={user.age}
                country={user.country}
              />
            </div>
          );
        })}
      </div>
    </>
  );
};

export default HomePage;
