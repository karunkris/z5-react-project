import SignupCard from "../components/SignupCard";
import { auth, db } from "../other/firebase";
import { doc, setDoc } from "firebase/firestore";
import { useState } from "react/cjs/react.development";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../other/AuthContext";
import Mounts from "../other/Mounts";

const SignupPage = () => {
  // loading variable to ensure that users don't spam the signup button
  const [loading, setLoading] = useState(false);

  // taking the signup user function from auth file
  const { signupUser } = useAuth();

  const mounted = Mounts();

  let navigate = useNavigate();

  const registerEmployee = async (employeeData, email, password) => {
    setLoading(true);

    try {
      // signing up the user with email and password
      await signupUser(email, password);

      // setting the uid as the key of the data in firestore
      await setDoc(doc(db, "employees", auth.currentUser.uid), employeeData);
      navigate("/home");
    } catch (error) {
      alert(error.message);
    }

    mounted.current && setLoading(false);
  };

  return (
    <div className="mainBackground">
      <SignupCard onRegister={registerEmployee} loading={loading} />
    </div>
  );
};

export default SignupPage;
