import { Link } from "react-router-dom";

const ViewDetails = (props) => {
  return (
    <div className="viewDetailsCard d-flex flex-column">
      <p>
        Full Name: <strong>{props.name}</strong>
      </p>
      <p>
        Email ID: <strong>{props.email}</strong>
      </p>
      <p>
        Age: <strong>{props.age}</strong>
      </p>
      <p>
        Gender: <strong>{props.gender}</strong>
      </p>
      <p>
        Location: <strong>{props.country}</strong>
      </p>
      <p>
        Skill set: <strong>{props.skills.toString()}</strong>
      </p>

      <div className="row">
        <div className="col-md-6">
          <Link to={"/edit"}>
            <button className="btn btn-warning">Update</button>
          </Link>
        </div>
        <div className="col-md-6">
          <button
            onClick={props.onCancel}
            className="btn-outline-secondary btn"
          >
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
};

export default ViewDetails;
