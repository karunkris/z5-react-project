import { Link } from "react-router-dom";
import { useRef, useState } from "react";

const SignupCard = (props) => {
  const nameRef = useRef();
  const emailRef = useRef();
  const passwordRef = useRef();
  const ageRef = useRef();
  const countryRef = useRef();

  const [genderValue, setGenderValue] = useState();
  const [skills, setSkills] = useState([]);

  const selectGender = (e) => {
    setGenderValue(e.target.value);
  };

  const selectSkills = (e) => {
    setSkills([...skills, e.target.value]);
  };

  // main submit function
  function signupUser(event) {
    event.preventDefault();

    if (genderValue) {
      const employeeData = {
        name: nameRef.current.value,
        age: ageRef.current.value,
        country: countryRef.current.value,
        gender: genderValue,
        skills: skills.length > 0 ? skills : "No skills set",
      };

      props.onRegister(
        employeeData,
        emailRef.current.value,
        passwordRef.current.value
      );
    } else {
      alert("Select your Gender");
    }
  }

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col">
          <div className="signupCard card">
            <div className="card-body d-flex flex-column justify-content-around">
              <h5 className="card-title text-center">SIGNUP</h5>

              <form className="d-flex flex-column" onSubmit={signupUser}>
                <input
                  type="text"
                  placeholder="Full Name"
                  className="border-0 border-bottom my-2"
                  ref={nameRef}
                  required
                />
                <input
                  type="email"
                  placeholder="Email"
                  className="mt-2 border-0 border-bottom mb-2"
                  ref={emailRef}
                  required
                />

                <input
                  type="password"
                  placeholder="Password"
                  className="mt-2 border-0 border-bottom mb-2"
                  ref={passwordRef}
                  required
                />

                <input
                  type="number"
                  placeholder="Age"
                  className="mt-3 border-0 border-bottom mb-2"
                  min={"0"}
                  max={"100"}
                  ref={ageRef}
                  required
                />

                {/* gender */}
                <div className="row mt-2">
                  <p className="mb-2 text-muted">Gender</p>
                  <div className="col-md-12">
                    <input
                      type="radio"
                      value="Male"
                      className="form-check-input"
                      id="Male"
                      name="gender"
                      onClick={selectGender}
                    />
                    <label htmlFor="Male" className="mx-2">
                      Male
                    </label>

                    <input
                      type="radio"
                      value="Female"
                      className="form-check-input"
                      id="Female"
                      name="gender"
                      onClick={selectGender}
                    />
                    <label htmlFor="Female" className="mx-2">
                      Female
                    </label>

                    <input
                      type="radio"
                      value="Other"
                      className="form-check-input"
                      id="other"
                      name="gender"
                      onClick={selectGender}
                    />
                    <label htmlFor="other" className="mx-2">
                      Other
                    </label>
                  </div>
                </div>

                {/* country */}
                <p className="mb-2 text-muted mt-3">Location</p>
                <select className="form-select" required ref={countryRef}>
                  <option defaultValue={null}>Choose Country</option>
                  <option value={"United States"}>United States</option>
                  <option value={"Canada"}>Canada</option>
                  <option value={"India"}>India</option>
                  <option value={"United Kingdom"}>United Kingdom</option>
                  <option value={"Japan"}>Japan</option>
                </select>

                {/* skills */}
                <div className="row mt-3">
                  <p className="mb-2 text-muted">Skills</p>
                  <div className="col-md-12 skills">
                    <input
                      type="checkbox"
                      value="React"
                      className="form-check-input"
                      id="react"
                      onChange={selectSkills}
                    />
                    <label htmlFor="react" className="mx-2">
                      REACT
                    </label>

                    <input
                      type="checkbox"
                      value="Python"
                      className="form-check-input"
                      id="python"
                      onChange={selectSkills}
                    />
                    <label htmlFor="python" className="mx-2">
                      Python
                    </label>

                    <input
                      type="checkbox"
                      value="Java"
                      className="form-check-input"
                      id="java"
                      onChange={selectSkills}
                    />
                    <label htmlFor="java" className="mx-2">
                      Java
                    </label>
                  </div>
                </div>

                <button
                  className="btn btn-warning my-4"
                  disabled={props.loading}
                >
                  Signup
                </button>
              </form>

              <div className="d-flex flex-row justify-content-center sub-info">
                <Link to={"/login"} className="linkStyle">
                  Already have an account? Login Here
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignupCard;
