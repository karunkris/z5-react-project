import { useRef, useState } from "react/cjs/react.development";
import { useAuth } from "../other/AuthContext";
import Alerts from "../other/Alerts";

const ForgotPass = (props) => {
  const emailRef = useRef();
  const [loading, setLoading] = useState(false);
  const [confirm, setConfirm] = useState(false);

  const { forgotPassword } = useAuth();

  const sendPassordReset = async () => {
    setLoading(true);
    try {
      await forgotPassword(emailRef.current.value);
      setConfirm(true);
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <div className="viewDetailsCard d-flex flex-column">
      <h3>Change your password</h3>
      <input
        type="email"
        placeholder="Enter your email"
        className="form-control mb-4 mt-3"
        ref={emailRef}
        required
      />
      {confirm && <Alerts message={"Please check your email"} />}
      <div className="row">
        <div className="col-md-6">
          <button
            className="btn btn-warning"
            onClick={sendPassordReset}
            disabled={loading}
          >
            Confirm
          </button>
        </div>
        <div className="col-md-6">
          <button
            onClick={props.onCancel}
            className="btn-outline-secondary btn"
          >
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
};

export default ForgotPass;
