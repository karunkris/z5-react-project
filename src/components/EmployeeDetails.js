const EmployeeDetails = (props) => {
  return (
    <div className="card text-white gradient-custom" style={{ width: "18rem" }}>
      <div
        className="card-header"
        style={{
          background: "rgb(248, 212, 83)",
          color: "black",
          fontWeight: "bold",
        }}
      >
        {props.name}
      </div>
      <ul className="list-group list-group-flush">
        <li className="list-group-item">{props.age}</li>
        <li className="list-group-item">{props.skills.toString()}</li>
        <li className="list-group-item">{props.country}</li>
      </ul>
    </div>
  );
};

export default EmployeeDetails;
