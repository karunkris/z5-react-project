import { Link } from "react-router-dom";
import { useRef, useState } from "react";
import ForgotPass from "./ForgotPassword";
import Backdrop from "./ForgotPassBackdrop";

const LoginCard = (props) => {
  const [passChangeIsOpen, setPassChangeOpen] = useState(false);

  const passReset = () => {
    setPassChangeOpen(true);
  };

  const closePassReset = () => {
    setPassChangeOpen(false);
  };

  // storing values of email and password to login
  const emailRef = useRef();
  const passwordRef = useRef();

  function loginUser(event) {
    event.preventDefault();
    props.onLogin(emailRef.current.value, passwordRef.current.value);
  }

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col">
          <div className="loginCard card">
            <div className="card-body d-flex flex-column justify-content-around">
              <h5 className="card-title text-center">LOGIN</h5>

              <form className="d-flex flex-column" onSubmit={loginUser}>
                <input
                  type="email"
                  placeholder="Email"
                  className="border-0 border-bottom"
                  ref={emailRef}
                  required
                />
                <input
                  type="password"
                  placeholder="Password"
                  className="mt-4 border-0 border-bottom mb-2"
                  ref={passwordRef}
                  required
                />
                <button
                  className="btn btn-warning mt-4"
                  disabled={props.loading}
                >
                  Login
                </button>
              </form>

              <div className="d-flex flex-column text-center justify-content-center sub-info">
                <p className="linkStyle" onClick={passReset}>
                  Forgot Password?
                </p>
                <Link to={"/signup"} className="linkStyle">
                  Don't have an account? Signup Here
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
      {passChangeIsOpen && <Backdrop onCancel={closePassReset} />}
      {passChangeIsOpen && <ForgotPass onCancel={closePassReset} />}
    </div>
  );
};

export default LoginCard;
