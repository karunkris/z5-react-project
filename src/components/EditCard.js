import { Link } from "react-router-dom";
import { useRef, useState } from "react/cjs/react.development";
import { useAuth } from "../other/AuthContext";

const EditCard = (props) => {
  const nameRef = useRef();
  const countryRef = useRef();
  const ageRef = useRef();
  const [skills, setSkills] = useState([]);

  const { activeUser } = useAuth();

  const selectSkills = (e) => {
    setSkills([...skills, e.target.value]);
  };

  function updateUser(event) {
    event.preventDefault();

    const updatedData = {
      name: nameRef.current.value,
      age: ageRef.current.value,
      country: countryRef.current.value,
      skills: skills.length > 0 ? skills : "No skills",
    };

    props.onUpdate(updatedData);
  }

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col">
          <div className="editCard card">
            <div className="card-body d-flex flex-column justify-content-around">
              <h5 className="card-title text-center">UPDATE PROFILE</h5>

              <form className="d-flex flex-column" onSubmit={updateUser}>
                <input
                  type="text"
                  placeholder="Full Name"
                  className="border-0 border-bottom my-2"
                  ref={nameRef}
                  required
                />
                <input
                  type="email"
                  placeholder="Email"
                  className="mt-2 border-0 border-bottom mb-2"
                  value={activeUser.email}
                  disabled
                  required
                />

                <input
                  type="number"
                  placeholder="Age"
                  className="mt-3 border-0 border-bottom mb-2"
                  min={13}
                  max={100}
                  ref={ageRef}
                  required
                />

                {/* country */}
                <p className="mb-2 text-muted mt-3">Location</p>
                <select className="form-select" required ref={countryRef}>
                  <option defaultValue={null}>Choose Country</option>
                  <option value={"United States"}>United States</option>
                  <option value={"Canada"}>Canada</option>
                  <option value={"India"}>India</option>
                  <option value={"United Kingdom"}>United Kingdom</option>
                  <option value={"Japan"}>Japan</option>
                </select>

                {/* skills */}
                <div className="row mt-3">
                  <p className="mb-2 text-muted">Skills</p>
                  <div className="col-md-12 skills">
                    <input
                      type="checkbox"
                      value="React"
                      className="form-check-input"
                      id="react"
                      name="react"
                      onChange={selectSkills}
                    />
                    <label htmlFor="react" className="mx-2">
                      REACT
                    </label>

                    <input
                      type="checkbox"
                      value="Python"
                      className="form-check-input"
                      id="python"
                      name="python"
                      onChange={selectSkills}
                    />
                    <label htmlFor="python" className="mx-2">
                      Python
                    </label>

                    <input
                      type="checkbox"
                      value="Java"
                      className="form-check-input"
                      id="java"
                      name="java"
                      onChange={selectSkills}
                    />
                    <label htmlFor="java" className="mx-2">
                      Java
                    </label>
                  </div>
                </div>

                <button className="btn btn-warning mt-4">Update</button>
              </form>

              <div className="d-flex flex-row justify-content-center sub-info">
                <Link to={"/home"} className="linkStyle">
                  Go back home
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditCard;
