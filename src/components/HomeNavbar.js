import ViewDetails from "./ViewDetails";
import Backdrop from "./ForgotPassBackdrop";
import { useState } from "react";
import { auth, db } from "../other/firebase";
import { doc, getDoc } from "firebase/firestore";
import { useAuth } from "../other/AuthContext";
import { Navigate, useNavigate } from "react-router-dom";

const HomeNavbar = () => {
  const [DetailsIsOpen, setDetailsOpen] = useState(false);
  const [name, setName] = useState("");
  const [age, setAge] = useState(0);
  const [gender, setGender] = useState("");
  const [country, setCountry] = useState("");
  const [skills, setSkills] = useState("");

  const { signoutUser, activeUser } = useAuth();

  let navigate = useNavigate();

  // retrieving data of the user that is currently signed in
  if (activeUser) {
    const employee = doc(db, "employees", activeUser.uid);
    getDoc(employee).then((doc) => {
      setName(doc.data().name);
      setGender(doc.data().gender);
      setCountry(doc.data().country);
      setAge(doc.data().age);
      setSkills(doc.data().skills);
    });
  } else {
    <Navigate to={"/login"} />;
  }

  const openDetails = () => {
    setDetailsOpen(true);
  };

  const closeDetails = () => {
    setDetailsOpen(false);
  };

  const logOut = async () => {
    try {
      await signoutUser();
      navigate("/login");
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <nav className="navbar navbar-dark" style={{ background: "black" }}>
      <span
        className="navbar-brand mx-4"
        style={{ color: "rgb(248, 212, 83)" }}
      >
        Hey, {name.split(" ")[0]}
      </span>
      <div className="d-flex gap-3 justify-content-md-end">
        <button className="btn btn-warning" onClick={openDetails}>
          View Profile
        </button>
        <button className="btn btn-outline-warning me-3" onClick={logOut}>
          Logout
        </button>
      </div>
      {DetailsIsOpen && <Backdrop onCancel={closeDetails} />}
      {DetailsIsOpen && (
        <ViewDetails
          onCancel={closeDetails}
          name={name}
          email={auth.currentUser.email}
          gender={gender}
          country={country}
          age={age}
          skills={skills}
        />
      )}
    </nav>
  );
};

export default HomeNavbar;
