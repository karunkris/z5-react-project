import { Navigate, Outlet } from "react-router-dom";
import { useAuth } from "./AuthContext";

const SignInRoute = () => {
  const { activeUser } = useAuth();
  return activeUser ? <Navigate to={"/home"} /> : <Outlet />;
};

export default SignInRoute;
