import { Route, Routes } from "react-router-dom";
import { lazy, Suspense } from "react";

import LoginPage from "../pages/LoginPage";
import SignupPage from "../pages/SignupPage";
import WelcomePage from "../pages/WelcomePage";
import PageNotFound from "../pages/PageNotFound";

import SignInRoute from "./SignInRoute";
import PrivateRoute from "./PrivateRoute";

const LazyHome = lazy(() => import("../pages/HomePage"));
const LazyEdit = lazy(() => import("../pages/EditPage"));

const Routing = () => {
  return (
    <Suspense fallback={<div className="loadingStyle"></div>}>
      <Routes>
        <Route element={<SignInRoute />}>
          <Route path="/" element={<WelcomePage />} />
          <Route path="/signup" element={<SignupPage />} />
          <Route path="/login" element={<LoginPage />} />
        </Route>
        <Route element={<PrivateRoute />}>
          <Route path="/edit" element={<LazyEdit />} />
          <Route path="/home" element={<LazyHome />} />
        </Route>
        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </Suspense>
  );
};

export default Routing;
