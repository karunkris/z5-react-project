import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  sendPasswordResetEmail,
  signInWithEmailAndPassword,
  signOut,
} from "firebase/auth";
import react, { useContext, useEffect } from "react";
import { useState } from "react/cjs/react.development";
import { auth } from "./firebase";

const AuthContext = react.createContext();

export const useAuth = () => {
  return useContext(AuthContext);
};

export const AuthProvider = ({ children }) => {
  const [activeUser, setActiveUser] = useState();
  const [loading, setLoading] = useState(true);

  const signupUser = (email, password) => {
    return createUserWithEmailAndPassword(auth, email, password);
  };

  const loginUser = (email, password) => {
    return signInWithEmailAndPassword(auth, email, password);
  };

  const signoutUser = () => {
    return signOut(auth);
  };

  const forgotPassword = (email) => {
    return sendPasswordResetEmail(auth, email);
  };

  useEffect(() => {
    const unsub = onAuthStateChanged(auth, (user) => {
      setActiveUser(user);
      setLoading(false);
    });
    return unsub;
  }, []);

  const value = {
    activeUser,
    loginUser,
    signupUser,
    signoutUser,
    forgotPassword,
  };

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  );
};
