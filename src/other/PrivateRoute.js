import { Navigate, Outlet } from "react-router-dom";
import { useAuth } from "./AuthContext";

const PrivateRoute = () => {
  const { activeUser } = useAuth();
  return activeUser ? <Outlet /> : <Navigate to={"/login"} />;
};

export default PrivateRoute;
